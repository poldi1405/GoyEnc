package yenc

import (
	"errors"
	"runtime"

	"github.com/pbnjay/memory"
)

var ErrChunksize = errors.New("invalid chunksize")

func getLimit(chunks int64, chunksize int64) (int64, error) {
	// default maximum at 128 goroutines; at 4 MiB per part that amounts to 512 MiB
	max := int64(128)

	// CPUs with only 4 Cores are rare nowadays so let's put the bar a bit lower for our seniors
	if runtime.NumCPU() < 4 {
		max = 64
	}

	// maximum of one goroutine per chunk
	if max > chunks {
		max = chunks
	}

	// make sure it's not exceeding 10% RAM usage
	RAM := int64(memory.TotalMemory())
	if RAM/10 < max*chunksize {
		if chunksize > RAM/10 {
			return -1, ErrChunksize
		}
		// Note: Tests might be unreliable.
		return RAM / 10 / chunksize, nil
	}

	return max, nil
}
